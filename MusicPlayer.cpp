#include <iostream>
#include <string.h>
#include<conio.h>
using namespace std;

struct node
{
	string number;
	node *next;
};
class list{
	public:
		bool Empty(node *head);

		char mainMenu();

		void insertAsFirstElement(node *&head, node *&last, string number);

		void insert(node *&head, node *&last, string number);

		void remove(node *&head, node *&last);

		void view(node *current);
};

bool Empty(node *head){
	
	if (head == NULL)
		return true;
	else 
		return false;
}

char mainMenu(){
	
	char choice;
	
	cout<<"1. Add Music to your Playlist"<<endl;
	cout<<"2. Delete Music to your Playlist"<<endl;
	cout<<"3. View your Playlist"<<endl;
	cout<<"4. Exit"<<endl;
	
	cin>>choice;
	
	return choice;
	
}

void insertAsFirstElement(node *&head, node *&last, string number){
	
	node *temp = new node;
	temp->number = number;
	temp->next = NULL;
	head = temp;
	last = temp;
}

void insert(node *&head, node *&last, string number){
	
	if (Empty(head)){
		insertAsFirstElement(head, last, number);
	}  
	else{
		node *temp = new node;
		temp->number = number;
		temp->next = NULL;
		last->next = temp;
		last = temp;
	}
}

void remove(node *&head, node *&last){
	
	if (Empty(head)){
		system("cls");
		cout<<"Spatipay Playlist is empty!\n"<<endl;
	}
	else if (head == last){
		delete head;
		head == NULL;
		last == NULL;
		system("cls");
	}
	else{
		node *temp = head;
		head = head->next;
		delete temp;
		system("cls");
	}
}

void view(node *current){
	if(Empty(current)){
		system("cls");
		cout<<"Spatipay Playlist is empty!\n"<<endl;
	}
	else{
		system("cls");
		cout<<"Spatipay Playlist: "<<endl;
		while (current != NULL){					
			cout<<current->number<< endl;
			cout<<endl;				
			current = current->next;					
		}
	}
	
}


int main (){
	
	
	node *head = NULL;
	node *last = NULL;
	int choice;
	string number;
	
	while (choice != '4'){
	
		choice = mainMenu();
		
		switch(choice){
			case '1': //add
				system("cls");
				cout<<"Enter the Music Title (Format: Scripted by Zayn): ";
				cin.ignore();
				getline(cin,number);
				insert(head, last, number);
				system("cls");
				break;
			case '2': //delete
				remove(head, last);
				break;
			case '3': //view
				view(head);
				break;
			default:
				break;
		}
	}
	
	return 0;
	
}
