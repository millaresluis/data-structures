hamCount = 0
sodaCount = 0
friesCount = 0
saladCount = 0
bbqCount = 0
icCount = 0

foodCart = []


while True:
    print("----------------------------------------")
    print("---Welcome to myFood Ordering System!---")
    print("----------------------------------------")
    print("1. Hamburger = P59")
    print("2. Soda      = P29")
    print("3. Fries     = P49")
    print("4. Salad     = P89")
    print("5. Barbeque  = P19")
    print("6. Ice Cream = P39")
    print("7. Proceed to Checkout")

    choice = int(input('Now, tell me what you want! \n'))

    if choice == 1:
        amount = int(input("How many Hamburgers would you like? \n"))
        hamCount += amount
        foodCart.append("Hamburger")
    
    elif choice == 2:
        amount = int(input("How many Sodas would you like? \n"))
        sodaCount += amount
        foodCart.append("Soda")
    
    elif choice == 3:
        amount = int(input("How many Fries would you like? \n"))
        friesCount += amount
        foodCart.append("Fries")
    
    elif choice == 4:
        amount = int(input("How many Salad would you like? \n"))
        saladCount += amount
        foodCart.append("Salad")
    
    elif choice == 5:
        amount = int(input("How many Barbeque would you like? \n"))
        bbqCount += amount
        foodCart.append("Barbeque")
    
    elif choice == 6:
        amount = int(input("How many Ice Cream would you like? \n"))
        icCount += amount
        foodCart.append("Ice Cream")
    
    elif choice == 7:
        hamTotal = (hamCount * 59)
        sodaTotal = (sodaCount * 29)
        friesTotal = (friesCount * 49)
        saladTotal = (saladCount * 89)
        bbqTotal = (bbqCount * 19)
        icTotal = (icCount * 39)
        total = hamTotal + sodaTotal + friesTotal + saladTotal + bbqTotal + icTotal
        
        
        print ("--------------------------------------------------------------------------------")
        print ("---Here is the summary of your order! Thanks for purchasing, enjoy your meal!---")
        print ("--------------------------------------------------------------------------------")
        foodCart.sort()
        for item in foodCart:
            print("###",item)
        
        print("---------------------------------------------------------")
        print("-------------------------RECEIPT-------------------------")
        print("---------------------------------------------------------")
        print("                 QTY                  PRICE                       ")
        print("# of BBQ:       ", bbqCount , "                  P" , bbqTotal )
        print("# of Fries:     ", friesCount , "                  P" , friesTotal )
        print("# of Hamburger: ", hamCount , "                  P" , hamTotal )
        print("# of Ice Cream: ", icCount , "                  P" , icTotal )
        print("# of Salad:     ", saladCount , "                  P" , saladTotal )
        print("# of Soda:      ", sodaCount , "                  P" , sodaTotal )
        print("TOTAL:          ","                    P",total)
        
        break
        

