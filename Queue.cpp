#include<iostream>
#include<conio.h>
#include<cstdlib>

using namespace std;

class List{
	private:
		typedef struct node
		{
			string title;
			string singer;
			node* next;
		}* 
		nodePtr;
		nodePtr head;
		nodePtr tail;
		nodePtr temp;
		nodePtr curr;
	public:
		List();
		void EnQueue(string addTitle, string addSinger);
		void DeQueue();
		void PrintQueue();
};

List::List()
{
	head = NULL;
	tail = NULL;
	temp = NULL;
	curr = NULL;
}

void List::EnQueue(string addTitle, string addSinger)
{
	system("cls");
	nodePtr n = new node;
	n->title = addTitle;
	n->singer = addSinger;
	n->next = NULL;
	if(head!=NULL){
		curr=head;
		while (curr->next!=NULL){
			curr=curr->next;
		}
		curr->next = n;
		tail = n;
	}
	else{
		head = n;
		tail = n;
	}
	
}

void List::DeQueue()
{
	if(head==NULL){
		cout<<"Queue is empty!\n";
	}
	else if(head->next == NULL){
		head = NULL;
		cout<<"Queue is now empty!\n";
	}
	else{
		curr = head;
		head = head->next;
		curr->next = NULL;
		cout<<"Removed!\n";
	}
}

void List::PrintQueue()
{
	if (head==NULL){
		cout<<"Nothing to display here!\n";
	}
	else{
		curr=head;
		while(curr!=NULL){
			cout << " Title: " << curr->title<<endl;
			cout <<" Artist: "<<curr->singer<< endl;
			cout<<"-------------------"<<endl;
			curr=curr->next;
		}
	}
}

List list;

void mainMenu()
{
	cout<<"1. Add to Queue"<<endl; 
	cout<<"2. Remove"<<endl; 
	cout<<"3. Display your Queue"<<endl;
	cout<<"Input: ";
	char c;
	cin>>c;
	cin.ignore(1,'\n');
	switch(c){
		case '1':{
			system ("cls");
			string addTitle, addSinger;
			cout<<"Enter the Song Title: ";
			getline(cin,addTitle);
			cout<<"Enter the Artist: ";
			getline(cin,addSinger);
			list.EnQueue(addTitle,addSinger);
			cout<<"Queue!\n";
			break;
		}
		case '2':
		{
			system ("cls");
			list.DeQueue();
			break;
		}
		case '3':
		{
			system ("cls");
			list.PrintQueue();
			break;
		}
		default:{
			cout<<"Invalid !\n";
			break;
		}
	}
	mainMenu();
}

int main()
{
	cout<<"-----QUEUE!!!-----"<<endl;	
	mainMenu();
}

