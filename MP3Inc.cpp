#pragma comment(lib,"winmm.lib")
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include <ctime>
using namespace std;

class list
{
	public:
		list();
		int Enqueue(int qData,int counter);
		int Dequeue();
		int AddToPlaylist(int addData, int counter);
		int RemoveFromPlaylist(int counter);
		void displayLibrary();
		void ViewPlaylist();
		void PrintQueue(int queueCounter);
		void PlayQueue(int queueCounter);
		void PlayStacks(int stackCounter);
		void SelectionSort();		
		void pushNode(string musicTitle,string musicArtist ,string musicGenre ,string musicAlbum,int dataGenre);
		
	private:
		typedef struct node
		{
			bool inPlaylist;
			bool inQueue;
			int data;
			int dataPlaylist;
			int dataQueue;
			int dataGenre;
			string musicTitle;
			string musicArtist;
			string musicGenre;
			string musicAlbum;			
			node* next;
		}* nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr top;

	
};


list::list()
{
	temp = NULL;
	head = NULL;
	curr = NULL;
	top = NULL;
}


void list::pushNode(string musicTitle,string musicArtist ,string musicGenre ,string musicAlbum, int dataGenre)
{
	nodePtr n = new node;
	n->next = NULL;
	if(head!=NULL)
	{
		curr = head;
		temp = head;
		while(curr->next != NULL)
		{
			curr = curr->next;
			temp = curr;
		}
		curr->next =n;
		curr = curr->next;
		curr->data = temp->data+1;
		system("CLS");
		cout << "Please wait... "<<endl;
	}
	else
	{
		head = n;
		n->data =0;
	}
	top = n;
	n->musicTitle = musicTitle;
	n->musicArtist = musicArtist;
	n->musicGenre = musicGenre;
	n->musicAlbum = musicAlbum;
	n->inPlaylist = false;
	n->inQueue = false;
	n->dataPlaylist =0;
	n->dataQueue =0;
	n->dataGenre = dataGenre;
}


void list::displayLibrary()
{
	
	for(int i = 1;i<=top->data;i++)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->data!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0)
		{
		}
		else
		{
			cout << curr->data << ". " << "TITLE: "<< curr->musicTitle<<endl
				 << "     ALBUM: " << curr->musicAlbum<<endl 
				 << "     GENRE: " <<curr->musicGenre <<endl
				 <<endl;
		}
	} 
	cout <<endl;
}

int list::AddToPlaylist(int addData, int counter)
{
	curr=head;
	temp=head;
	while(curr != NULL && curr-> data != addData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		cout << addData << "Invalid Input! \n";
		return 0;
	}
	else if(curr->inPlaylist==true)
	{
		return 0;
	}
	else
	{	
		curr->dataPlaylist = counter;
		curr->inPlaylist =true;
		return 1; 
	}
}

void list::ViewPlaylist()
{
	
	cout << "CURRENT PLAYLIST: " << endl;
	cout<<endl;
	for(int i = top->data;i>=0;i--)
	{
		curr=head;
		temp=head;
		while(curr!=NULL&&curr->dataPlaylist!=i)
		{
			curr = curr->next;
		}
		if(curr==NULL||curr->data==0||curr->dataPlaylist==0||curr->inPlaylist==false)
		{
		}
		else
		{
			cout << curr->dataPlaylist << ". " <<"  TITLE: "<< curr->musicTitle<<endl 
				 << "     ALBUM: " << curr->musicAlbum<<endl
				 << "     GENRE: " <<curr->musicGenre <<endl
				 <<endl;
		}
	}

	cout<< endl;
}

int list::RemoveFromPlaylist(int counter)
{
	int delData = counter -1;
	curr=head;
	temp=head;
	while(curr != NULL && curr-> dataPlaylist != delData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		return 0;
	}
	else if(curr->inPlaylist==false)
	{
		return 0;
	}
	else
	{	
		cout << "REMOVED"<< endl;
		curr->dataPlaylist = 0;
		curr->inPlaylist =false;
		return 1; 
	}
}

void list::PlayStacks(int stackCounter)
{
	int i=1;
	while(i<=stackCounter)
	{
	
		system("CLS");
		
		if (stackCounter==1)
		{
			cout << "EMPTY" <<endl;
		}
		else 
		{
			
			for(int x = top->data;x>=0;x--)
			{
				curr=head;
				temp=head;
				while(curr!=NULL&&curr->dataPlaylist!=x)
				{
					curr = curr->next;
				}
				if(curr==NULL||curr->data==0||curr->dataPlaylist==0||curr->inPlaylist==false)
				{
					
				}
				else
				{
					cout << curr->dataPlaylist << ". " <<"  TITLE: " << curr->musicTitle<<endl
						 << "     ALBUM: " << curr->musicAlbum<<endl
						 << "     GENRE: " <<curr->musicGenre << endl
						 <<endl;
				}
			}
		}
		
		curr=head;
		while(i-1!=curr->dataPlaylist)
		{
			curr=curr->next;
		}
		curr=curr->next;
		int choice;
		cout << endl
			 << "1. |> Next Song"<< endl
			 << "2. X  Main Menu"<< endl	
			 << " "<<endl
			 <<"Now Playing: "<< curr->musicTitle <<endl;
		cin >> choice;
		if(choice==1)
		{
			i=i+1;
		}
		else
		{
			break;
		}	
	}
}

int list::Enqueue(int qData,int counter)
{
	curr=head;
	temp=head;
	while(curr != NULL && curr-> data != qData)
	{
		temp = curr;
		curr = curr->next;
	}
	if (curr == NULL or curr->data ==0)
	{
		cout << "Invalid Input!\n";
		return 0;
	}
	else if(curr->inQueue==true)
	{
		return 0;
	}
	else
	{	
		curr->dataQueue = counter;
		curr->inQueue =true;
		return 1; 
	}
}

int list::Dequeue()
{
	curr=head;
	while(curr->next!=NULL)
	{
		if(curr->dataQueue!=0)
		{
			curr->dataQueue=curr->dataQueue-1;
			if(curr->dataQueue==0)
			{
				curr->inQueue=false;			
			}
		}
		curr=curr->next;
	}
	return 1;
}

void list::PrintQueue(int queueCounter)
{
	cout << "CURRENTLY ON QUEUE: " <<endl;
	if(queueCounter==1)
	{
		cout<<endl;
		cout << "Queue is empty! " <<endl;
	}
	else
	{
		for(int i=0;i<=top->data;i++)
		{
			curr=head;
			temp=head;
			while(curr!=NULL&&curr->dataQueue!=i)
			{
				curr = curr->next;
			}
			if(curr==NULL||curr->data==0||curr->dataQueue==0||curr->inQueue==false)
			{
			}
			else
			{
				cout << curr->dataQueue << ". " <<"TITLE: " << curr->musicTitle<<endl
					 << "   ALBUM: " << curr->musicAlbum<<endl
					 << "   GENRE: " <<curr->musicGenre << endl
					 <<endl;
			}
		}	
	}
	cout<<endl;
}

void list::PlayQueue(int queueCounter)
{
	int i=1;
	while(i<=queueCounter)
	{
		system("CLS");
		
		if(queueCounter==1)
		{
			cout << "EMPTY" <<endl;
		}
		else
		{
			for(int x=0;x<=top->data;x++)
			{
				curr=head;
				temp=head;
				while(curr!=NULL&&curr->dataQueue!=x)
				{
					curr = curr->next;
				}
				if(curr==NULL||curr->data==0||curr->dataQueue==0||curr->inQueue==false)
				{
				}
				else
				{
					cout << curr->dataQueue << ".) " <<"TITLE: " << curr->musicTitle<<endl
						 << "     ALBUM: " << curr->musicAlbum<<endl
						 << "     GENRE: " <<curr->musicGenre<<endl
						 <<endl;
				
				}
				
			}	
		}
		cout<<endl;
		
		curr=head;
		while(i-1!=curr->dataQueue)
		{
			curr=curr->next;
		}
		curr=curr->next;
		int choice;
		cout << "1. |> Next Song"<< endl
			 << "2. X  Main Menu"<< endl	
			 << " "<<endl
			 <<"Now Playing: "<< curr->musicTitle <<endl;
			 															  			 
			 
		cin >> choice;
		if(choice==1)
		{
			i=i+1;
		}
		else
		{
			break;
		}	
	}	
}

void list::SelectionSort()
{
	int i = 1;
	for(i;i<=10;i++)
	{
		curr=head;
		temp=head;
		while(curr->next!=NULL)
		{
			if(curr->dataGenre == i)
			{
				cout << "GENRE: " << curr->musicGenre<<endl
				     << "TITLE: " << curr->musicTitle<<endl
				     << "ALBUM: " << curr->musicAlbum<<endl
				     <<endl;
			}
			curr=curr->next;
		}
	}
}

int main () 
{	
	list MP3;
	int playListMusic=1;
	int queueMusic=1;
	bool input = true;
	//Mind Of Mine
	MP3.pushNode("PILLOWTALK - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("iTs YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("BeFoUr - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("sHe - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("dRuNk - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("rEaR vIeW - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("wRoNg - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("fOoL fOr YoU - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("BoRdErZ - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("tRuTh - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("lUcOzAdE - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("TiO - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("BLUE - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("BRIGHT - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("LIKE I WOULD - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("SHE DONT LOVE ME - ZAYN", "ZAYN", "POP", "Mind of Mine",5);
	MP3.pushNode("","","","",0);
	//FOUR
	MP3.pushNode("Steal My Girl - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Ready to Run - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Where Do Broken Hearts Go - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("18 - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Girl Almighty - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Fool�s Gold - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Night Changes - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("No Control - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Fireproof - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Spaces - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Stockholm Syndrome - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Clouds - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Change Your Ticket - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Illusion - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Once in a Lifetime - One Direction", "One Direction", "POP", "FOUR",5);
	MP3.pushNode("Act My Age - One Direction", "One Direction", "POP", "FOUR",5);
	//PERIPHERAL VISION 10
	MP3.pushNode("Dizzy On The Comedown - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Cutting My Fingers Off - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Take My Head - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Diazepam - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("New Scream - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Threshold - TurnOver","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Humming - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Like Slowly Disappearing - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("I Would Hate You If I Could - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Intrapersonal - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	//FOREST HILLS DRIVE 2014 13
	MP3.pushNode("Intro - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("January 28th - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Wet Dreamz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("03' Adolescence' - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("A tale of two Citiez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("FireSquad - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("St Tropez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Hello - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("G.O.M.D - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("No Role Modelz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Apparently - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Love Yourz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Note To Self - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	// WE ARE NOT YOUR KIND 14
	MP3.pushNode("Nero Forte - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Spiders - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);	
	MP3.pushNode("Insert Coin - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Unsainted - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("A Lier's Funeral - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Solway Firth - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Birth Of the Cruel - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Orphan - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Critical Darling - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("My Pain - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Red Flag - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Not Long for this world - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Death because of death - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("What's Next? - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	//Enema of the State 12
	MP3.pushNode("Dumpweed - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Dont Leave Me - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Aliens Exist - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Going Away To College - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("What's My Age Again' - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Dysentry Gary - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Adam's Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("All The Small Things - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("The Party Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Mutt - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Wendy Clear - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Anthem - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	//American FootBall 9
	MP3.pushNode("Never Meant - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("The Summer Ends - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("Honestly - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("For Sure - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("You Know I Should Be Leaving Soon - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("But The Regrets Are Killing Me - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("I'll See You When We're Both Not So Emotional - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("Stay Home - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("The One With The Wurlitzer - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	//LNOTGY 10
	MP3.pushNode("Citezens Of Earth - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("I Hope This Comes Back To Haunt You - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("December - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Smooth Seas Don�t Make Good Sailors - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Cant Kick Up The Roots - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Gold Steps - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Kali Ma - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Rock Bottom - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("The Beach is For Lovers Not Losers - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Serpents - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	//Doo-Wops & Hooligans 11
	MP3.pushNode("Grenade - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Just the Way you Are - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Our First Time - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Runaway Baby - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("The Lazy Song - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Marry You - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Talking to the Moon - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Liquor Store Blues - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Count on Me - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("The Other Side - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Somewhere in Brooklyn - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	//Cutterpillow  13
	MP3.pushNode("Superproxy - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Back2me - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Waiting for the Bus - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Fine Time - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Kama Supra - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Overdrive - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Slo Mo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Torpedo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Huwag mo nang Itanong - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Paru-Parong Ningning - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Walang Nagbago - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Cutterpillow - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Poorman's Grave - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	//  Save Rock & Roll
	MP3.pushNode("The Phoenix - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("My Songs Know What You Did in the Dark (Light Em Up) - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Alone Together - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Where Did the Party Go - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Just One Yesterday - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("The Mighty Fall - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Miss Missing You - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Death Valley - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Young Volcanoes - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Rat a Tat - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Save Rock and Roll - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	// Clarity
	MP3.pushNode("Hourglass - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Shave it - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Spectrum - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Lost at Sea - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Clarity - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Codec - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Stache - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Fall in to Sky - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Follow you Down - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Epos - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Stay The Night - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Push Play - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Alive - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Breakin a Sweat - Zedd", "Zedd", "POP", "Clarity",5);
	//NOthing Personal
	MP3.pushNode("Weightless - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Break Your Little Heart - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Damned If I Do Ya, Damned If I Don't - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Lost In Stereo - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Stella - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Sick Little Games - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Hello, Brooklyn - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Walls - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Too Much - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Keep The Change, You Filthy Animal - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("A Party Song (The Walk Of Shame) - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Therapy - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	//All We Know is Falling
	MP3.pushNode("Pressure - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("All We Know - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Emergency - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Brighter - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Here We Go Again - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Let This Go - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Woah - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Conspiracy - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Franklin - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("My Heart - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	//If You Were a Movie, This Would Be Your Soundtrack
	MP3.pushNode("Scene One - James Dean & Audrey Hepburn - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	MP3.pushNode("Scene Two - Roger Rabbit - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	MP3.pushNode("Scene Three - Stomach Tied In Knots - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	MP3.pushNode("Scene Four - Don't You Ever Forget About Me - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	MP3.pushNode("Scene Five - With Ears To See and Eyes To Hear - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	//The Finer Things (Acoustic)
	MP3.pushNode("Elevated - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Deadly Conversation - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Hard To Please - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Prepare to be Noticed - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Over The Line - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Sample Existence - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Remedy - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Nothing's Wrong - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Mind Bottled - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Critical - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Easy Enough - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	//Science & Faith
	MP3.pushNode("Nothing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("If you ever come back - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Science & Faith - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Long Gone and Moved on - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Deadman Walking - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("This Love - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Walk Away - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Exit Wounds - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("You won't feel a Thing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	//Move Along
	MP3.pushNode("Dirty Little Secret - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Stab My Back - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Move Along - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("It Ends Tonight - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Change Your Mind - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Night Drive - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("11:11 PM - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Dance Inside - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Top of the World - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Straitjacket Feeling  - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("I'm waiting - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Can't take it - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	//Sweetener
	MP3.pushNode("God Is A Woman - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Sweetener - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Breathin - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("No Tears Left To Cry - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Everytime - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Get Well Soon- Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Goodnight n Go - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("R.E.M. - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("The Light is Coming - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	//Red
	MP3.pushNode("State of Grace - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("Red - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("I Knew You Were Trouble - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("All Too Well - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("22- Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("I Almost Do - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("We Are Never Ever Getting Back Together - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("Starlight - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("Holy Ground - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("Begin Again- Taylor Swift", "Taylor Swift", "POP", "Red",5);
	//Hopeless Fountain Kingdom
	MP3.pushNode("Eyes Closed - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Alone - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Now or Never - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Sorry - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Bad At Love - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Devil In Me - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	//Honey Works
	MP3.pushNode("Assertion of the Heart - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Bae Love - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Bloom in Love Color	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Brazen Honey - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Can I confess to you? - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("The day I knew Love	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Declaration of the Weak	 - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Dream Fanfare - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Fansa - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("I like you now - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Inokori-sensei - HoneyWorks feat. flower", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Light Proof Theory - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Love meets and love continues - HoneyWorks feat. Kotoha", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Maidens	- HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Mama - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Monday's Melancholy - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Nostalgic Rainfall - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Picture Book of my first love - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Secret of Sunday - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Sick name love wazurai - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("A small lion - HoneyWorks feat. Minami & LIPxLIP", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Tokyo summer session - HoneyWorks feat. Gumi & flower", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Twins - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	//Multiply
	MP3.pushNode("One - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("I'm a mess - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Sing - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Don't - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Nina - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Photograph - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Bloodstream - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Terenife Sea - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Thinking out loud - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	//Divide
	MP3.pushNode("Eraser - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Castle on the hill - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Dive - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Shape of you - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Perfect - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Galway Girl - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Happier - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("New man - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Hearts don't break around here - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("What do i know - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("How do you feel - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Supermarket Flowers - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Barcelona - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Bibia be ye ye - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Nancy Mulligan - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Save yourself - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	//1989
	MP3.pushNode("Welcome To New York - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Blank Space - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Style - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Out Of The Woods - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("All you had to do was stay - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Shake it Off - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("I wish you would - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Bad Blood - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Wildest Dreams - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("How You Get The Girl - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("This Love - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("I know Places - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Clean - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	//After Laughter
	MP3.pushNode("Hard Times - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Rose-Colored Boy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Told You So - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Forgiveness - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Fake Happy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("26 - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Pool - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Grudges - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Caught In the Middle - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Idle Worship - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("No Friend - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Tell Me How - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	//Need You Now
	MP3.pushNode("Need You Now - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Our Kind Of Love - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("American Honey - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Hello World - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Perfect Day - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Love This Pain - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("When You Got A Good Thing - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Stars Tonight - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("If I Know Then - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Something Bout A Woman - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	//HeartBreak
	MP3.pushNode("Home - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Heart Break - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Somebody's Else's Heart - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("This City - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Hurt - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Army - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Good Time To Be Alive - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Think About You - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Big Love In A Small Town - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	//Symphony Soldier
	MP3.pushNode("Angel With A Shotgun - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Endlessly  - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Animal - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Intoxicated - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Lala - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Her Love Is My Religion - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Another Me - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Lovesick Fool - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	//Freudian
	MP3.pushNode("Get You - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Best Part - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Hold Me DOwn - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Nue Roses - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Loose - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("We Find Love - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Blessed - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Take Me Away - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Transform - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Freudian - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	//Case Study 01
	MP3.pushNode("LOVE AGAIN - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("SUPER POSITION - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("TOO DEEP TO TURN BACK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("FRONTAL LOBE MUZIK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("CYANIDE - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("ARE YOU OKAY? - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("OPEN UP - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("ENTROPY - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("RESTORE THE FEELING - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("COMPLEXITIES - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("loveAgain - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	// Natty Dread
	MP3.pushNode("Lively Up Yourself - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Them Belly Full - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Rebel MP3 - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("So Jah Seh - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Natty Dread - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Bend Down Low - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Talkin Blues - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Ravolution - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	//Legend
	MP3.pushNode("Is This Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Could You Be Loved - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Three Little Birds - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Buffalo Soldier - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Get Up Stand Up- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Stir It Up - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("One Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("I Shot The Sheriff - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Waiting In Vain - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Redemption Song - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Satisfy My Soul - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Exodus - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Jamming - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	//Rastaman Vibrations
	MP3.pushNode("Positive Vibrations - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Roots, Rocks, Reggae - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Johnny Was - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Cry To Me - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Want More - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Crazy Bald Head - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Who The Cap Fit- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Night Shift - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("War - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Rat Race - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	//4 your Eyez only
	MP3.pushNode("4 Your Eyez Only - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Change - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Deja Vu - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Foldin Clothes - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("For Whom The Bells Toll - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Immortal - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Neighbors - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("She's Mine Pt. 1 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("She's Mine Pt. 2 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Ville Mentality - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	//Russ The Mixtape
	MP3.pushNode("For the Stunt - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Aint NobodyTakin My Baby - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("2 A.M. - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Down For You - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Off The Strength - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("We Just Havent Met Yet - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Always Knew- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Whenever- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Waste My Time- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Lost- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Yung God- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Lately- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Comin' Thru- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Connected- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Keep the Faith- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("All My Angels- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Brush Me (feat. Colliee Buddz)- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1); 
	//December Avenue (2017)
	MP3.pushNode("City Lights - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Sleep Tonight - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Fallin� - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Ears and Rhymes - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("I�ll Be Watching You - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Back To Love - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Eroplanong Papel - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Dive - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Forever - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Breathe Again - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Time To Go - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	//Too Weird To Live, Too Rare To Die!
	MP3.pushNode("Casual Affair - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	MP3.pushNode("Collar Full  - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	MP3.pushNode("Girl That You Love - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	MP3.pushNode("Miss Jackson - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	MP3.pushNode("Nicotine - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	MP3.pushNode("The End Of All Things - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	MP3.pushNode("This Is Gospel - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	MP3.pushNode("Vegas Lights - Panic! At The Disco", "Panic! At The Disco", "POP-ROCK", "Too Weird To Live, Too Rare To Die!",7);
	//Master Of Puppets
	MP3.pushNode("Battery - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	MP3.pushNode("Damage, Inc. - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	MP3.pushNode("Disposable Heroes - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	MP3.pushNode("Leper Messiah - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	MP3.pushNode("Master Of Puppets - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	MP3.pushNode("Orion - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	MP3.pushNode("The Thing That Should Be - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	MP3.pushNode("Welcome Home (Sanitarium) - METALLICA", "METALLICA", "METAL", "Master Of Puppets",6);
	//Get Your Heart On!
	MP3.pushNode("You Suck at Love - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Can't Keep My Hands off You - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Jet Lag - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Astronaut - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Loser of the Year - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Anywhere Else But Here - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Freaking Me Out - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Summer Paradise - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Gone Too Soon - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("Last One Standing - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	MP3.pushNode("This Song Saved My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Get Your Heart On!",3);
	//Still Not Getting Any
	MP3.pushNode("Shut Up! - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Welcome to My Life - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Perfect World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Thank You - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Me Against the World - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Jump - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Everytime - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Promise - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("One - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Untitled - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	MP3.pushNode("Perfect - Simple Plan", "Simple Plan", "POP-PUNK", "Still Not Getting Any",3);
	//Vessels
	MP3.pushNode("Ode To sleep - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Migraine - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("House Of gold - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("car Radio - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Semi Automatic - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("SCreen - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("The Run and Go - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Fake You Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Guns for Hands - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Trees - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Truce - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	//Blurry Face
	MP3.pushNode("Heavy Dirty Soul - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Stressed Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Ride - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Fairly Local - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Tear in my heart - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Lane Boy - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("The Judge - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Doubt - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Polarize - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("We Dont Believe What's on TV - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Message Man - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Home Town - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Not Today - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Goner - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);

	MP3.pushNode("Dizzy On The Comedown - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Cutting My Fingers Off - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Take My Head - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Diazepam - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("New Scream - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Threshold - TurnOver","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Humming - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Like Slowly Disappearing - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("I Would Hate You If I Could - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	MP3.pushNode("Intrapersonal - Turnover","Turnover","ALTERNATIVE","Peripheral Vision",4);
	//FOREST HILLS DRIVE 2014 13
	MP3.pushNode("Intro - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("January 28th - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Wet Dreamz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("03' Adolescence' - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("A tale of two Citiez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("FireSquad - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("St Tropez - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Hello - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("G.O.M.D - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("No Role Modelz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Apparently - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Love Yourz - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	MP3.pushNode("Note To Self - J Cole", "JCOLE", "HIPHOP", "2014 Forrest Hills Drive",1);
	// WE ARE NOT YOUR KIND 14
	MP3.pushNode("Nero Forte - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Spiders - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);	
	MP3.pushNode("Insert Coin - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Unsainted - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("A Lier's Funeral - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Solway Firth - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Birth Of the Cruel - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Orphan - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Critical Darling - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("My Pain - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Red Flag - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Not Long for this world - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("Death because of death - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	MP3.pushNode("What's Next? - SLIPKNOT", "SLIPKNOT", "METAL", "We Are Not Your Kind",6);
	//Enema of the State 12
	MP3.pushNode("Dumpweed - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Dont Leave Me - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Aliens Exist - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Going Away To College - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("What's My Age Again' - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Dysentry Gary - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Adam's Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("All The Small Things - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("The Party Song - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Mutt - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Wendy Clear - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	MP3.pushNode("Anthem - Blink 182", "Blink 182", "POP-PUNK", "Enema of the State",3);
	//American FootBall 9
	MP3.pushNode("Never Meant - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("The Summer Ends - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("Honestly - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("For Sure - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("You Know I Should Be Leaving Soon - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("But The Regrets Are Killing Me - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("I'll See You When We're Both Not So Emotional - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("Stay Home - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("The One With The Wurlitzer - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	//LNOTGY 10
	MP3.pushNode("Citezens Of Earth - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("I Hope This Comes Back To Haunt You - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("December - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Smooth Seas Don�t Make Good Sailors - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Cant Kick Up The Roots - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Gold Steps - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Kali Ma - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Rock Bottom - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("The Beach is For Lovers Not Losers - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Serpents - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	//Doo-Wops & Hooligans 11
	MP3.pushNode("Grenade - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Just the Way you Are - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Our First Time - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Runaway Baby - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("The Lazy Song - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Marry You - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Talking to the Moon - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Liquor Store Blues - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Count on Me - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("The Other Side - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Somewhere in Brooklyn - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	//Cutterpillow  13
	MP3.pushNode("Superproxy - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Back2me - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Waiting for the Bus - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Fine Time - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Kama Supra - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Overdrive - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Slo Mo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Torpedo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Huwag mo nang Itanong - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Paru-Parong Ningning - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Walang Nagbago - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Cutterpillow - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Poorman's Grave - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	//  Save Rock & Roll
	MP3.pushNode("The Phoenix - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("My Songs Know What You Did in the Dark (Light Em Up) - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Alone Together - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Where Did the Party Go - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Just One Yesterday - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("The Mighty Fall - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Miss Missing You - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Death Valley - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Young Volcanoes - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Rat a Tat - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Save Rock and Roll - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	// Clarity
	MP3.pushNode("Hourglass - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Shave it - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Spectrum - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Lost at Sea - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Clarity - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Codec - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Stache - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Fall in to Sky - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Follow you Down - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Epos - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Stay The Night - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Push Play - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Alive - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Breakin a Sweat - Zedd", "Zedd", "POP", "Clarity",5);
	//NOthing Personal
	MP3.pushNode("Weightless - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Break Your Little Heart - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Damned If I Do Ya, Damned If I Don't - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Lost In Stereo - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Stella - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Sick Little Games - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Hello, Brooklyn - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Walls - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Too Much - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Keep The Change, You Filthy Animal - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("A Party Song (The Walk Of Shame) - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	MP3.pushNode("Therapy - All Time Low", "All Time Low", "POP-PUNK", "Nothing Personal",3);
	//All We Know is Falling
	MP3.pushNode("Pressure - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("All We Know - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Emergency - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Brighter - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Here We Go Again - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Let This Go - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Woah - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Conspiracy - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Franklin - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("My Heart - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	//If You Were a Movie, This Would Be Your Soundtrack
	MP3.pushNode("Scene One - James Dean & Audrey Hepburn - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	MP3.pushNode("Scene Two - Roger Rabbit - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	MP3.pushNode("Scene Three - Stomach Tied In Knots - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	MP3.pushNode("Scene Four - Don't You Ever Forget About Me - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	MP3.pushNode("Scene Five - With Ears To See and Eyes To Hear - Sleeping With Sirens", "Sleeping With Sirens", "ACOUSTIC", "If You Were a Movie, This Would Be Your Soundtrack",8);
	//The Finer Things (Acoustic)
	MP3.pushNode("Elevated - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Deadly Conversation - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Hard To Please - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Prepare to be Noticed - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Over The Line - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Sample Existence - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Remedy - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Nothing's Wrong - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Mind Bottled - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Critical - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	MP3.pushNode("Easy Enough - State Champs", "State Champs", "ACOUSTIC", "The Finer Things (Acoustic)",8);
	//Science & Faith
	MP3.pushNode("Nothing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("If you ever come back - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Science & Faith - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Long Gone and Moved on - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Deadman Walking - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("This Love - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Walk Away - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("Exit Wounds - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	MP3.pushNode("You won't feel a Thing - The Script", "The Script", "ALTERNATIVE", "Science & Faith",4);
	//Move Along
	MP3.pushNode("Dirty Little Secret - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Stab My Back - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Move Along - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("It Ends Tonight - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Change Your Mind - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Night Drive - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("11:11 PM - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Dance Inside - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Top of the World - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Straitjacket Feeling  - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("I'm waiting - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	MP3.pushNode("Can't take it - The All-American Reject", "The All-American Reject", "POP-PUNK", "Move Along",3);
	//Sweetener
	MP3.pushNode("God Is A Woman - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Sweetener - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Breathin - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("No Tears Left To Cry - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Everytime - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Get Well Soon- Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("Goodnight n Go - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("R.E.M. - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	MP3.pushNode("The Light is Coming - Ariana Grande", "Ariana Grande", "POP", "Sweetener",5);
	//Red
	MP3.pushNode("State of Grace - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("Red - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("I Knew You Were Trouble - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("All Too Well - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("22- Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("I Almost Do - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("We Are Never Ever Getting Back Together - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("Starlight - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("Holy Ground - Taylor Swift", "Taylor Swift", "POP", "Red",5);
	MP3.pushNode("Begin Again- Taylor Swift", "Taylor Swift", "POP", "Red",5);
	//Hopeless Fountain Kingdom
	MP3.pushNode("Eyes Closed - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Alone - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Now or Never - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Sorry - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Bad At Love - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	MP3.pushNode("Devil In Me - Hasley", "Hasley", "INDIE", "Hopeless Fountain Kingdom",11);
	//Honey Works
	MP3.pushNode("Assertion of the Heart - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Bae Love - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Bloom in Love Color	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Brazen Honey - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Can I confess to you? - HoneyWorks feat. South", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("The day I knew Love	- HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Declaration of the Weak	 - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Dream Fanfare - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Fansa - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("I like you now - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Inokori-sensei - HoneyWorks feat. flower", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Light Proof Theory - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Love meets and love continues - HoneyWorks feat. Kotoha", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Maidens	- HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Mama - HoneyWorks", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Monday's Melancholy - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Nostalgic Rainfall - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Picture Book of my first love - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Secret of Sunday - HoneyWorks feat. Miku & Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Sick name love wazurai - HoneyWorks feat. Gumi", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("A small lion - HoneyWorks feat. Minami & LIPxLIP", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Tokyo summer session - HoneyWorks feat. Gumi & flower", "HoneyWorks", "ROCK", "HoneyWorks",12);
	MP3.pushNode("Twins - HoneyWorks feat. Miku", "HoneyWorks", "ROCK", "HoneyWorks",12);
//Vessels
	MP3.pushNode("Ode To sleep - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Migraine - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("House Of gold - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("car Radio - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Semi Automatic - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("SCreen - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("The Run and Go - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Fake You Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Guns for Hands - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Trees - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Truce - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	MP3.pushNode("Holding on to you - Twenty One Pilots", "Twenty One Pilots", "EMO", "Vessels",10);
	//Blurry Face
	MP3.pushNode("Heavy Dirty Soul - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Stressed Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Ride - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Fairly Local - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Tear in my heart - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Lane Boy - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("The Judge - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Doubt - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Polarize - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("We Dont Believe What's on TV - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Message Man - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Home Town - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Not Today - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Goner - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	//Multiply
	MP3.pushNode("One - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("I'm a mess - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Sing - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Don't - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Nina - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Photograph - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Bloodstream - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Terenife Sea - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	MP3.pushNode("Thinking out loud - Ed Sheeran", "Ed Sheeran", "POP", "Multiply",5);
	//Divide
	MP3.pushNode("Eraser - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Castle on the hill - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Dive - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Shape of you - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Perfect - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Galway Girl - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Happier - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("New man - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Hearts don't break around here - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("What do i know - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("How do you feel - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Supermarket Flowers - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Barcelona - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Bibia be ye ye - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Nancy Mulligan - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	MP3.pushNode("Save yourself - Ed Sheeran", "Ed Sheeran", "POP", "Divide",5);
	//1989
	MP3.pushNode("Welcome To New York - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Blank Space - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Style - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Out Of The Woods - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("All you had to do was stay - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Shake it Off - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("I wish you would - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Bad Blood - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Wildest Dreams - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("How You Get The Girl - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("This Love - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("I know Places - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	MP3.pushNode("Clean - Taylor Swift", "Taylor Swift", "POP", "1989",5);
	//After Laughter
	MP3.pushNode("Hard Times - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Rose-Colored Boy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Told You So - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Forgiveness - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Fake Happy - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("26 - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Pool - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Grudges - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Caught In the Middle - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Idle Worship - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("No Friend - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	MP3.pushNode("Tell Me How - Paramore", "Paramore", "ALTERNATIVE", "After Laughter",4);
	//Need You Now
	MP3.pushNode("Need You Now - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Our Kind Of Love - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("American Honey - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Hello World - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Perfect Day - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Love This Pain - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("When You Got A Good Thing - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Stars Tonight - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("If I Know Then - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	MP3.pushNode("Something Bout A Woman - Lady Antibellum", "Lady Antibellum", "COUNTRY", "Need You Now",13);
	//HeartBreak
	MP3.pushNode("Home - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Heart Break - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Somebody's Else's Heart - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("This City - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Hurt - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Army - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Good Time To Be Alive - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Think About You - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	MP3.pushNode("Big Love In A Small Town - Lady Antibellum", "Lady Antibellum", "COUNTRY", "HeartBreak",13);
	//Symphony Soldier
	MP3.pushNode("Angel With A Shotgun - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Endlessly  - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Animal - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Intoxicated - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Lala - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Her Love Is My Religion - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Another Me - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	MP3.pushNode("Lovesick Fool - The Cab", "The Cab", "ALTERNATIVE", "Symphony Soldier",4);
	//Freudian
	MP3.pushNode("Get You - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Best Part - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Hold Me DOwn - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Nue Roses - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Loose - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("We Find Love - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Blessed - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Take Me Away - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Transform - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	MP3.pushNode("Freudian - Daniel Caesar", "Daniel Caesar", "R&B", "Freudian",2);
	//Case Study 01
	MP3.pushNode("LOVE AGAIN - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("SUPER POSITION - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("TOO DEEP TO TURN BACK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("FRONTAL LOBE MUZIK - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("CYANIDE - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("ARE YOU OKAY? - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("OPEN UP - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("ENTROPY - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("RESTORE THE FEELING - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("COMPLEXITIES - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	MP3.pushNode("loveAgain - Daniel Caesar", "Daniel Caesar", "R&B", "Case Study 01",2);
	// Natty Dread
	MP3.pushNode("Lively Up Yourself - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Them Belly Full - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Rebel MP3 - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("So Jah Seh - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Natty Dread - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Bend Down Low - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Talkin Blues - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	MP3.pushNode("Ravolution - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Natty Dread",9);
	//Legend
	MP3.pushNode("Is This Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("No Woman No Cry - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Could You Be Loved - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Three Little Birds - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Buffalo Soldier - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Get Up Stand Up- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Stir It Up - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("One Love - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("I Shot The Sheriff - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Waiting In Vain - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Redemption Song - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Satisfy My Soul - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Exodus - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	MP3.pushNode("Jamming - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Legend",9);
	//Rastaman Vibrations
	MP3.pushNode("Positive Vibrations - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Roots, Rocks, Reggae - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Johnny Was - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Cry To Me - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Want More - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Crazy Bald Head - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Who The Cap Fit- Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Night Shift - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("War - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	MP3.pushNode("Rat Race - Bob Marley and the Wailers", "Bob Marley and the Wailers", "REGGAE", "Rastaman Vibrations",9);
	//4 your Eyez only
	MP3.pushNode("4 Your Eyez Only - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Change - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Deja Vu - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Foldin Clothes - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("For Whom The Bells Toll - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Immortal - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Neighbors - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("She's Mine Pt. 1 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("She's Mine Pt. 2 - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	MP3.pushNode("Ville Mentality - J Cole", "JCOLE", "HIPHOP", "4 your Eyez only",1);
	//Russ The Mixtape
	MP3.pushNode("For the Stunt - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Aint NobodyTakin My Baby - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("2 A.M. - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Down For You - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Off The Strength - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("We Just Havent Met Yet - Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Always Knew- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Whenever- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Waste My Time- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Inbetween- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Lost- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Yung God- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Lately- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Comin' Thru- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Connected- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Keep the Faith- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("All My Angels- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	MP3.pushNode("Brush Me (feat. Colliee Buddz)- Russ The Mixtape", "Russ", "HIPHOP", "Russ The Mixtape",1);
	//December Avenue (2017)
	MP3.pushNode("City Lights - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Sleep Tonight - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Fallin� - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Ears and Rhymes - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("I�ll Be Watching You - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Back To Love - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	MP3.pushNode("Eroplanong Papel - December Avenue", "December Avenue", "ALTERNATIVE", "December Avenue (2017)",4);
	//American FootBall 9
	MP3.pushNode("Never Meant - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("The Summer Ends - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("Honestly - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("For Sure - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("You Know I Should Be Leaving Soon - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("But The Regrets Are Killing Me - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("I'll See You When We're Both Not So Emotional - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("Stay Home - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	MP3.pushNode("The One With The Wurlitzer - American FootBall", "American FootBall", "EMO", "American FootBall",10);
	//LNOTGY 10
	MP3.pushNode("Citezens Of Earth - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("I Hope This Comes Back To Haunt You - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("December - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Smooth Seas Don�t Make Good Sailors - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Cant Kick Up The Roots - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Gold Steps - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Kali Ma - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Rock Bottom - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("The Beach is For Lovers Not Losers - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	MP3.pushNode("Serpents - Neck Deep", "Neck Deep", "POP-PUNK", "LNOTGY",3);
	//Doo-Wops & Hooligans 11
	MP3.pushNode("Grenade - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Just the Way you Are - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Our First Time - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Runaway Baby - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("The Lazy Song - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Marry You - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Talking to the Moon - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Liquor Store Blues - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Count on Me - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("The Other Side - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	MP3.pushNode("Somewhere in Brooklyn - Bruno Mars", "Bruno Mars", "POP", "Doo-Wops & Hooligans",5);
	//Cutterpillow  13
	MP3.pushNode("Superproxy - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Back2me - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Waiting for the Bus - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Fine Time - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Kama Supra - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Overdrive - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Slo Mo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Torpedo - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Huwag mo nang Itanong - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Paru-Parong Ningning - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Walang Nagbago - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Cutterpillow - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	MP3.pushNode("Poorman's Grave - EraserHeads", "EraserHeads", "POP", "CutterPillow",5);
	//  Save Rock & Roll
	MP3.pushNode("The Phoenix - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("My Songs Know What You Did in the Dark (Light Em Up) - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Alone Together - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Where Did the Party Go - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Just One Yesterday - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("The Mighty Fall - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Miss Missing You - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Death Valley - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Young Volcanoes - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Rat a Tat - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	MP3.pushNode("Save Rock and Roll - Fallout Boy", "Fallout Boy", "POP-ROCK", "Save Rock and Roll",7);
	// Clarity
	MP3.pushNode("Hourglass - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Shave it - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Spectrum - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Lost at Sea - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Clarity - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Codec - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Stache - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Fall in to Sky - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Follow you Down - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Epos - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Stay The Night - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Push Play - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Alive - Zedd", "Zedd", "POP", "Clarity",5);
	MP3.pushNode("Breakin a Sweat - Zedd", "Zedd", "POP", "Clarity",5);
	//All We Know is Falling
	MP3.pushNode("Pressure - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("All We Know - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Emergency - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Brighter - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Here We Go Again - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Let This Go - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Woah - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Conspiracy - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("Franklin - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	MP3.pushNode("My Heart - Paramore", "Paramore", "POP-ROCK", "All We Know is Falling",7);
	//Blurry Face
	MP3.pushNode("Heavy Dirty Soul - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Stressed Out - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Ride - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Fairly Local - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Tear in my heart - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Lane Boy - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("The Judge - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Doubt - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Polarize - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("We Dont Believe What's on TV - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Message Man - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	MP3.pushNode("Home Town - Twenty One Pilots", "Twenty One Pilots", "EMO", "Blurry Face",10);
	
	while(input == true)
	{
		cin.clear();
		system("CLS");
		int choice; 
		cout	<< "*   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  *"<<endl;
		cout	<< "*   MMM       MMM           PPPPPPPPPPPPP              333333333333333   *"<<endl;
		cout	<< "*   MMMM     MMMM           PPP          PPP           333        3333   *"<<endl;
		cout	<< "*   MMMM     MMMM           PPP          PPP                      3333   *"<<endl;
		cout	<< "*   MMMMMM MMMMMM           PPP          PPP                3333333333   *"<<endl;
		cout	<< "*   MMMMMMMMMMMMM           PPPPPPPPPPPPP                   3333333333   *"<<endl;
		cout	<< "*   MMM  MMM  MMM           PPP                                   3333   *"<<endl;
		cout	<< "*   MMM   M   MMM           PPP                        333        3333   *"<<endl;
		cout	<< "*   MMM       MMM           PPP                        333333333333333   *"<<endl;
		cout	<< "*                                                                        *"<<endl;
		cout	<< "*  PPPPP      LLL         AAAAAAA     YYYY   YYYY    EEEEEEE   RRRRRRR   *"<<endl;
		cout	<< "*  PP  PPP    LLL        AA     AA     YYY  YYYY     EE        RR    RR  *"<<endl;
		cout	<< "*  PPPPP      LLL        AAAAAAAAA      YYYYYYY      EEEE      RRRRRRR   *"<<endl;
		cout	<< "*  PP         LLLLLLL    AA     AA       YYYY        EE        RR  RRR   *"<<endl;
		cout	<< "*  PP         LLLLLLL    AA     AA     YYYYY         EEEEEEE   RR    RR  *"<<endl;
		cout	<< "*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *"<<endl;
		cout	<< "* - - - - - - - - - - - - - - 1. Music Library - - - - - - - - - - - - - *"<< endl;
		cout	<< "* - - - - - - - - - - - - - - 2. Queue   - - - - - - - - - - - - - - - - *"<< endl;
		cout	<< "* - - - - - - - - - - - - - - 3. Playlist  - - - - - - - - - - - - - - - *"<< endl;
		cout	<< "* - - - - - - - - - - - - - - 4. Selection Sort  - - - - - - - - - - - - *"<< endl;	
															  
		cin >> choice;
		
		
//LIBRARY
		if (choice == 1)
		{
			system("CLS");			
			MP3.displayLibrary();			
			cout <<endl;
			system("PAUSE");
		}
		
//QUEUE
		else if (choice == 2)
		{	
			system("CLS");  
			MP3.PrintQueue(queueMusic); 
			int playlistChoice;
			int musChoice;
			cout <<"1. Add to Queue" << endl
				 <<"2. Remove" <<endl
				 <<"3. Play" <<endl
				 <<"4. Main Menu" << endl;
			cin >> playlistChoice;
			if(cin.fail()||playlistChoice>4||playlistChoice<1)
			{ 
				cout << "Invalid Input";
				system("PAUSE");
			}
			else if(playlistChoice==1)
			{  
				MP3.displayLibrary();
				cout << "Number:  ";
				cin >> musChoice;
				queueMusic = MP3.Enqueue(musChoice,queueMusic) + queueMusic;
				system("PAUSE");
			}
			else if(playlistChoice==2)
			{
				//REMOVE FROM QUEUE / DEQUEUE
				queueMusic -= MP3.Dequeue();
				if(queueMusic<=0)
				{
					queueMusic=1;
				}
			}
			else if(playlistChoice==3)
			{
				MP3.PlayQueue(queueMusic);
			}
			else if(playlistChoice==4)
			{
				system("PAUSE");
			}
			else if(playlistChoice==5)
			{
				return 0;
			}
		}
		
//PLAYLIST
		else if (choice == 3)
		{
		    system("CLS");  
			MP3.ViewPlaylist(); 
			int playlistChoice;
			int musChoice;
			cout <<"1. Add to Playlist" << endl
				 <<"2. Remove" <<endl
				 <<"3. Play" <<endl
				 <<"4. Main Menu" << endl;
			cin >> playlistChoice;
			if(cin.fail()||playlistChoice>4||playlistChoice<1)
			{  
				cout << "Invalid Input";
				system("PAUSE");
			}
			else if(playlistChoice==1)
			{  
				MP3.displayLibrary();  
				cout << "Number: ";
				cin >> musChoice;
				playListMusic = MP3.AddToPlaylist(musChoice,playListMusic)+playListMusic;
				system("PAUSE");
			}
			else if(playlistChoice==2)
			{
				playListMusic =playListMusic-MP3.RemoveFromPlaylist(playListMusic);
			}
			else if(playlistChoice==3)
			{
				MP3.PlayStacks(playListMusic);
			}
			else if(playlistChoice==4)
			{
				system("PAUSE");
			}
			else
			{
			}
			
		}
		
//SORT
		else if (choice == 4)
		{
			system("CLS");
			MP3.SelectionSort();
			system("PAUSE");
		}
		else if (cin.fail())
		{
			cin.clear();
			cin.ignore();
		}
		else 
		{
		}
	
	int xRan;
	srand( time(0));
	xRan=rand()%10+1;
	
	
	}
} 	
	
