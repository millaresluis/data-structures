#include<iostream>
#include<cstdlib>
#include<conio.h>
#include<stdio.h>

using namespace std;

class List{
	private:
		typedef struct node{
			int num;
			string title;
			string singer;
			node* next;
			
		}*nodePtr;
		
		nodePtr head;
		nodePtr curr;
		nodePtr temp;
		nodePtr top;
	
	public:
		List();
		void Push(string addTitle, string addSinger);
		void Pop();
		void Display();
		string DisplayTop();
};

List::List()
{
	head=NULL;
	curr=NULL;
	temp=NULL;
	top = NULL;
}
void List::Push(string addTitle, string addSinger)
{
	system("cls");
	nodePtr n = new node;
	n->title = addTitle;
	n->singer = addSinger;
	n->next = NULL;
	if(head!=NULL){
		curr=head;
		while(curr->next!=NULL){
			curr = curr->next;
		}
		curr->next = n;
		n->num=top->num+1;
		curr=curr->next;
	}
	else{
		head=n;
		n->num=1;
	}
	top = n;
}

void List::Pop()
{
	curr = head;
	temp = head;
	if (head==NULL){
		cout<<"Stack is empty!"<<endl;
	}
	else if(curr->next==NULL){
		head=NULL;
		cout<<"Stack is now empty!\n";
	}
	else{
		while(curr->next!=NULL){
			temp = curr;
			curr = curr->next;
		}
		top = temp;
		top->next=NULL;
		cout<<"Removed\n";
	}
}

void List::Display()
{
	if(head!=NULL){
		for(int i = top->num;i>0;i--)
		{
			curr=head;
			while(curr!=NULL&&curr->num!=i)
			{
				curr = curr->next;
			}
			
			cout << curr->num << ".) Title: " << curr->title<<endl;
			cout <<"    Artist: "<<curr->singer<< endl;
			cout<<"-------------------"<<endl;
		}
	}
	else{
		system("cls");
		cout<<"Nothing to display here!\n";
	}
}


List list;

void mainMenu()
{
	cout<<"1. Push an Element"<<endl; 
	cout<<"2. Pop an Element"<<endl; 
	cout<<"3. Display your Stacks"<<endl;
	cout<<"Input: ";
	char c;
	cin>>c;
	cin.ignore(1,'\n');
	switch (c){
		case '1':{
			system("cls");
			string addTitle, addSinger;
			cout<<"Enter the Song Title: ";
			getline(cin,addTitle);
			cout<<"Enter the Artist: ";
			getline(cin,addSinger);
			list.Push(addTitle, addSinger);
			system("cls");
			cout<<"Pushed!\n";
			break;
		}
		case '2':{
			system("cls");
			list.Pop();
			break;
		}
		case '3':{
			system ("cls");
			list.Display();
			break;
		}
		default:{
			cout<<"Invalid !\n";
			break;
		}
	}
	mainMenu();
}


int main()
{	
    cout<<"-----STACKS!!!-----"<<endl;
	mainMenu();
}

